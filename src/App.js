import React, { Suspense } from 'react';

import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import './App.css';
import PrivateRoute from './js/components/PrivateRoute'


const Dashboard = React.lazy(() => import('./js/containers/Dashboard'));




function App() {

  return (
    <div>
      <Suspense fallback={<div>Loading...</div>}>
        <Router>
          <Switch>
            <Route exact path="/" component={Dashboard} />
            {/* <Route path="/register" component={Register} /> */}
            <PrivateRoute path="/dashboard" component={Dashboard} />
             {/*<PrivateRoute path="/dashboard" component={Dashboard} />
            <PrivateRoute path="/watched" component={WatchedMovies} /> */}
          </Switch>
        </Router>
      </Suspense>
    </div>
  );
}

export default App;

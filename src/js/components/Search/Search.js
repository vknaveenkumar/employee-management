import React, { useState, useEffect } from 'react';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import IconButton from '@material-ui/core/IconButton';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { fade } from '@material-ui/core/styles';
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
        margin: '20px 50px'
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        height: 28,
        margin: 4,
    },
    formControl: {
      //  margin: theme.spacing(3.0),
        minWidth: 120,
    },
});


function Filter(props) {

    const { classes } = props

    const [type, setType] = useState('name')
    const [search, setSearch] = useState('')

    useEffect(() => {
        props.filter(type, search)
    }, [type, search])


    const onChangeType = (e) => {
        setType(e.target.value)
    }

    const onChangeSearch = (e) => {
        setSearch(e.target.value)
    }



    return (
        <div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
                <Paper component="form" className={classes.root}>
                    <InputBase
                        className={classes.input}
                        placeholder="Search Movies"
                        inputProps={{ 'aria-label': 'search google maps' }}
                        onChange={onChangeSearch}
                    />
                    <IconButton type="submit" className={classes.iconButton} aria-label="search">
                        <SearchIcon />
                    </IconButton>


                </Paper>
                <FormControl variant="outlined" className={classes.formControl}>
                    <InputLabel id="demo-simple-select-outlined-label">Type</InputLabel>
                    <Select
                        //  labelId="demo-simple-select-outlined-label"
                        //id="demo-simple-select-outlined"
                        style ={{backgroundColor:'white '}}
                        value={type}
                        onChange={onChangeType}
                        label="Type"
                    >
                        <MenuItem value={'name'}>Name</MenuItem>
                        <MenuItem value={'genres'}>Genres</MenuItem>
                    </Select>
                </FormControl>

            </div>

        </div>
    );
}


export default withStyles(styles)(Filter);
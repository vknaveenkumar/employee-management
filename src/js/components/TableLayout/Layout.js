import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import TableCell from '@material-ui/core/TableCell';
import Cell from '../Table/Cell'
import { TablePagination } from '@material-ui/core';
import TableRow from '@material-ui/core/TableRow';
import { withStyles } from "@material-ui/core/styles";

import Table from '../Table/Table'

const styles = theme => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },

});


const sort_by = (field, reverse, primer) => {
    const key = primer ?
        function (x) {
            return primer(x[field])
        } :
        function (x) {
            return x[field]
        };

    reverse = !reverse ? 1 : -1;

    return function (a, b) {
        return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
    }
}




export class Layout extends Component {

    constructor(props) {
        super(props)
        this.state = {
            sortField: 'name',
            sortType: 'asc',
            page: 0,
            rowsPerPage: 5,
            dummyData: [
                {
                    name: 'Naveen Kumar',
                    designation: 'SSE',
                    band: 1.2,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 4.5,
                    relevantExperience: 3,
                    hackerRankTest: true,
                    hackerRankDate: '01-08-2020',
                },
                {
                    name: 'Gowtham',
                    designation: 'Lead',
                    band: 2,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 7.5,
                    relevantExperience: 5,
                    hackerRankTest: true,
                    hackerRankDate: '06-08-2020',
                },
                {
                    name: 'Anitha',
                    designation: 'Architect',
                    band: 3,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 10,
                    relevantExperience: 7,
                    hackerRankTest: false,
                    hackerRankDate: '',
                },
                {
                    name: 'b',
                    designation: 'SSE',
                    band: 1.2,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 4.5,
                    relevantExperience: 3,
                    hackerRankTest: true,
                    hackerRankDate: '01-08-2020',
                },
                {
                    name: 'c',
                    designation: 'Lead',
                    band: 2,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 7.5,
                    relevantExperience: 5,
                    hackerRankTest: true,
                    hackerRankDate: '06-08-2020',
                },
                {
                    name: 'd',
                    designation: 'Architect',
                    band: 3,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 10,
                    relevantExperience: 7,
                    hackerRankTest: false,
                    hackerRankDate: '',
                },
                {
                    name: 'e',
                    designation: 'SSE',
                    band: 1.2,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 4.5,
                    relevantExperience: 3,
                    hackerRankTest: true,
                    hackerRankDate: '01-08-2020',
                },
                {
                    name: 'f',
                    designation: 'Lead',
                    band: 2,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 7.5,
                    relevantExperience: 5,
                    hackerRankTest: true,
                    hackerRankDate: '06-08-2020',
                },
                {
                    name: 'g',
                    designation: 'Architect',
                    band: 3,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 10,
                    relevantExperience: 7,
                    hackerRankTest: false,
                    hackerRankDate: '',
                },
                {
                    name: 'h',
                    designation: 'SSE',
                    band: 1.2,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 4.5,
                    relevantExperience: 3,
                    hackerRankTest: true,
                    hackerRankDate: '01-08-2020',
                },
                {
                    name: 'i',
                    designation: 'Lead',
                    band: 2,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 7.5,
                    relevantExperience: 5,
                    hackerRankTest: true,
                    hackerRankDate: '06-08-2020',
                },
                {
                    name: 'j',
                    designation: 'Architect',
                    band: 3,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 10,
                    relevantExperience: 7,
                    hackerRankTest: false,
                    hackerRankDate: '',
                },
                {
                    name: 'k',
                    designation: 'SSE',
                    band: 1.2,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 4.5,
                    relevantExperience: 3,
                    hackerRankTest: true,
                    hackerRankDate: '01-08-2020',
                },
                {
                    name: 'l',
                    designation: 'Lead',
                    band: 2,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 7.5,
                    relevantExperience: 5,
                    hackerRankTest: true,
                    hackerRankDate: '06-08-2020',
                },
                {
                    name: 'm',
                    designation: 'Architect',
                    band: 3,
                    skills: 'React',
                    doj: '04-04-2020',
                    totalExperience: 10,
                    relevantExperience: 7,
                    hackerRankTest: false,
                    hackerRankDate: '',
                },
            ]
        }
    }


    _handleSort = (name, cellType, sortType) => {
        const { dummyData } = this.state

        let tableData = dummyData.sort(sort_by(name, sortType == 'asc' ? false : true))

        this.setState({
            sortField: name,
            sortType: sortType,
            dummyData: tableData
        })
    }


    renderTableHeader = () => {
        const { sortField, sortType } = this.state
        return (
            <TableRow style={{ color: 'white !important' }} >
                <TableCell> Select</TableCell>
                <Cell cellType='text' sortField={sortField} sortType={sortType} handleSort={this._handleSort} name='name' style={{ width: '100px' }}>Name</Cell>
                <Cell cellType='text' sortField={sortField} sortType={sortType} handleSort={this._handleSort} name='designation' style={{ width: '100px' }}>Designation</Cell>
                <Cell cellType='text' sortField={sortField} sortType={sortType} handleSort={this._handleSort} name='band' style={{ width: '10px' }}>Band</Cell>
                <Cell cellType='text' sortField={sortField} sortType={sortType} handleSort={this._handleSort} name='skills' style={{ width: '60px' }}>Skills</Cell>
                <Cell cellType='text' sortField={sortField} sortType={sortType} handleSort={this._handleSort} name='doj' style={{ width: '80px' }}>DOJ</Cell>
                <Cell cellType='text' sortField={sortField} sortType={sortType} handleSort={this._handleSort} name='totalExperience' style={{ width: '50px' }}>Experience</Cell>
                <Cell cellType='text' sortField={sortField} sortType={sortType} handleSort={this._handleSort} name='relevantExperience' style={{ width: '50px' }}>Relevent Experience</Cell>
                <Cell cellType='text' sortField={sortField} sortType={sortType} handleSort={this._handleSort} name='hackerRankTest' style={{ width: "10px" }}> Hacker Rank</Cell>
                <Cell cellType='text' sortField={sortField} sortType={sortType} handleSort={this._handleSort} name='hackerRankDate' >Hacker Rank Date</Cell>

            </TableRow>
        )
    }


    onSelectCandidate = (i) => {
        const { dummyData } = this.state

        if (dummyData[i].checked == undefined || dummyData[i].checked == false) {
            dummyData[i].checked = true;
        } else {
            dummyData[i].checked = false;
        }

        this.setState({
            dummyData
        })
    }

    renderTableBody = (statements) => {

        let { dummyData, page, rowsPerPage } = this.state;

        let tableData = dummyData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)



        return (
            <React.Fragment>
                {
                    tableData.map((statement, i) => (
                        <TableRow key={statement.transactionId}>
                            <TableCell ><input type='checkbox' onChange={() => { this.onSelectCandidate((page * rowsPerPage) + i) }} checked={statement.checked} /></TableCell>
                            <TableCell>{statement.name}</TableCell>
                            <TableCell >  {statement.designation}</TableCell>
                            <TableCell >{statement.band}</TableCell>
                            <TableCell >{statement.skills}</TableCell>
                            <TableCell >{statement.doj}</TableCell>
                            <TableCell >{statement.totalExperience}</TableCell>
                            <TableCell >{statement.relevantExperience}</TableCell>
                            <TableCell ><input type='checkbox' disabled={true} checked={statement.hackerRankTest} /></TableCell>
                            <TableCell >{statement.hackerRankTest && statement.hackerRankDate}</TableCell>
                        </TableRow>

                    ))
                }
            </React.Fragment>
        )
    }

    handleChangePage = (event, page) => {
        this.setState({
            page
        })
    };

    handleChangeRowsPerPage = (event) => {
        this.setState({
            page: 0,
            rowsPerPage: event.target.value
        })
    };


    render() {
        const { classes } = this.props
        const { page, rowsPerPage, dummyData } = this.state
        return (
            <React.Fragment>
                <Table classes={classes} tableHeader={this.renderTableHeader()} tableBody={this.renderTableBody()} />
                <TablePagination
                    rowsPerPageOptions={[5, 10]}
                    component="div"
                    count={dummyData.length}
                    page={page}
                    onChangePage={this.handleChangePage}
                    rowsPerPage={rowsPerPage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
            </React.Fragment>
        );
    }
}

export default withStyles(styles, { withTheme: true })(Layout);


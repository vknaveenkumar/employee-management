import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        height: '550px',
        overflow:'auto'
    },
}));
/**
 * 
 * @param {*} props 
 * Tab Layout for 4 tbs
 */
export default function SimpleTabs(props) {
    const classes = useStyles();
  
    const { currentTab,
        tabOneLabel, tabOneLayout,
        tabTwoLabel, tabTwoLayout,
        tabThreeLabel, tabThreeLayout,
        tabFourLabel, tabFourLayout
    } = props

    

    return (
        <div className={classes.root}>
            <AppBar position="static" style={{backgroundColor:props.backgroundColor}}>
                <Tabs value={currentTab}  aria-label="simple tabs example">
                    <Tab label={tabOneLabel} {...a11yProps(0)} />
                    <Tab label={tabTwoLabel} {...a11yProps(1)} />
                    <Tab label={tabThreeLabel} {...a11yProps(2)} />
                    <Tab label={tabFourLabel} {...a11yProps(3)} />
                </Tabs>
            </AppBar>
            <TabPanel value={currentTab} index={0}>
                {tabOneLayout}
            </TabPanel>
            <TabPanel value={currentTab} index={1}>
                {tabTwoLayout}
            </TabPanel>
            <TabPanel value={currentTab} index={2}>
                {tabThreeLayout}
            </TabPanel>
            <TabPanel value={currentTab} index={3}>
                {tabFourLayout}
            </TabPanel>
        </div>
    );
}
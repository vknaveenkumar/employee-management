import React from 'react';
import renderer from 'react-test-renderer';
import { mount, shallow } from 'enzyme';
import TextField from '@material-ui/core/TextField';
import Dashboard from '../Dashboard';

describe('Login Form', () => {
   
    it('Check submit button', () => {
        const wrapper = mount(<Dashboard />);
        expect(wrapper.find('button')).toHaveLength(1);
    });
});


});

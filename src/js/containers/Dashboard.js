import React from 'react';
import Navbar from '../components/Navbar/Navbar'
import { withStyles } from "@material-ui/core/styles";
import Background from '../../images/1.jpg'
import Container from '@material-ui/core/Container';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';
import TableLayout from '../components/TableLayout/Layout'

import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { BASE_URL } from '../../config/env'


/**
 * @param {*} theme 
 * This is Landing Dashboard Component
 */


const styles = theme => ({
    root: {
        width: '100%',
        marginTop: '50px'
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    paper: {
        //marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',

        // [theme.breakpoints.up('lg')]: {
        backgroundColor: 'white',
        padding: '20px',
        borderRadius: '30px'
        //},

    },
    pageBackground: {
        backgroundImage: 'url(' + Background + ')',
        height: '100vh',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',

        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'normal',
        alignItems: 'center',
    },
    table: {
        minWidth: 300,
        '& thead': {
            backgroundColor: 'mediumseagreen',
            '& tr ': {
                '& th ': {
                    color: 'white !important'
                }
            }
        },
        '& tbody >tr:nth-child(even)': {
            backgroundColor: '#dddddd'
        },
        '& tbody >tr:nth-child(odd)': {
            backgroundColor: 'white'
        }
    },

});




export class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            statements: [],
            dummyData : [{
                name : 'Naveen Kumar',
                designation: 'SSE',
                band : 1.2,
                skills:'React',
                doj:'04-04-2020',
                totalExperience : 4.5,
                relevantExperience:3,
                hackerRankTest : true,
                hackerRankDate : '01-08-2020',
            },
            {
                name : 'Gowtham',
                designation: 'Lead',
                band : 2,
                skills:'React',
                doj:'04-04-2020',
                totalExperience : 7.5,
                relevantExperience:5,
                hackerRankTest : true,
                hackerRankDate : '06-08-2020',
            },
            {
                name : 'Anitha',
                designation: 'Architect',
                band : 3,
                skills:'React',
                doj:'04-04-2020',
                totalExperience : 10,
                relevantExperience:7,
                hackerRankTest : false,
                hackerRankDate : '',
            }]
        }
    }

    componentDidMount() {

    }


   



    render() {
        const { classes } = this.props
        const { statements } = this.state

        return (
            <React.Fragment>
                <Navbar
                    appBarColor={'rgb(138,198,58)'}
                    appTitle='Employee Management'
                    {...this.props}
                />
                <Container >
                   
                    <div className={classes.root}>
                        {
                            <React.Fragment>
                               
                                <TableLayout classes={classes}  />
                              
                            </React.Fragment>
                        }
                    </div>

                </Container>

            </React.Fragment>
        );
    }
}


export default withStyles(styles, { withTheme: true })(Login);
